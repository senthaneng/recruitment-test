<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
    {!! Form::label('name', 'Product Name', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']) !!}
        <p class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</p>
    </div>
</div>
<div class="form-group {{ ($errors->has('quantity')) ? 'has-error' : '' }}">
    {!! Form::label('quantity', 'Product Quantity', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::text('quantity', null, ['class' => 'form-control', 'placeholder' => 'quantity']) !!}
        <p class="help-block">{{ ($errors->has('quantity') ? $errors->first('quantity') : '') }}</p>
    </div>
</div>
<div class="form-group {{ ($errors->has('price')) ? 'has-error' : '' }}">
    {!! Form::label('price', 'Product Price', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-4">
        {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'price']) !!}
        <p class="help-block">{{ ($errors->has('price') ? $errors->first('price') : '') }}</p>
    </div>
</div>