@extends('layouts.app')
@section('content')
    <ul class="breadcrumb">
        <li>{!! link_to_route('home.index', 'Home ') !!}</li>
        <li>{!! link_to_route('product.index', 'products ') !!}</li>
        <li class="active">Edit</li>
    </ul>
    {!! Form::model($product, ['url' => route('product.update', ['product' => $product->id]), 'method' => 'PATCH', 'role' => 'form', 'class' => 'form-horizontal']) !!}
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <span class="pull-left"><h4>Edit product</h4></span>
            <span class="pull-right">
                @include('product.back-btn', ['text' => 'Back'])
            </span>
        </div>
        <div class="panel-body">
            @include('product.form')
        </div>
        <div class="panel-footer text-right">
            @include('product.submit-btn', ['text' => 'Update', 'class' => 'blue'])
        </div>
    </div>
    {!! Form::close() !!}
@endsection