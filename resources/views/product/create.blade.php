@extends('layouts.app')
@section('content')
    <ul class="breadcrumb">
        <li>{!! link_to_route('home.index', 'Home') !!}</li>
        <li class="active">Create</li>
    </ul>
    {!! Form::open(['url' => route('product.index'), 'role' => 'form', 'class' => 'form-horizontal']) !!}

    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <span class="pull-left"><h4>Create product</h4></span>
            <span class="pull-right">
                @include('product.back-btn', ['text' => 'Back'])
            </span>
        </div>
        <div class="panel-body">
            @include('product.form')
        </div>
        <div class="panel-footer text-right">
            @include('product.submit-btn', ['text' => 'Create', 'class' => 'green'])
        </div>
    </div>
    {!! Form::close() !!}
@endsection