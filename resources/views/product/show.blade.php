@extends('layouts.app')
@section('content')
    <ul class="breadcrumb">
        <li>{!! link_to_route('home.index', 'Home ') !!}</li>
        <li>{!! link_to_route('product.index', 'product ') !!}</li>
        <li class="active">Full Details</li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <span class="pull-left"><h4>Full Details</h4></span>
            <span class="pull-right">
                @include('product.back-btn', ['text' => 'Back'])
            </span>
        </div>
        <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <th class="col-sm-2">Product Name</th>
                        <td>{{ $product->name }}</td>
                    </tr>
                    <tr>
                        <th>Price</th>
                        <td>{{ $product->price }}</td>
                    </tr>
                    <tr>
                        <th>Quantity</th>
                        <td>{{ $product->quantity }}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <td>{{ $product->total }}</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection