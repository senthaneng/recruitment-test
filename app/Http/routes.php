<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();


$router->group(['middleware' => 'auth'], function ($router) {
    $router->get('/', ['uses' => 'HomeController@index', 'as' => 'home.index']);

    $router->group(['prefix' => 'product'], function ($router) {
        $router->get('/', ['uses' => 'ProductionController@show', 'as' => 'product.index']);
        $router->get('{product}', ['uses' => 'ProductionController@show', 'as' => 'product.show']);
        $router->get('create', ['uses' => 'ProductionController@create', 'as' => 'product.create']);
        $router->post('/', ['uses' => 'ProductionController@store', 'as' => 'product.store']);
        $router->get('{product}/edit', ['uses' => 'ProductionController@edit', 'as' => 'product.edit']);
        $router->patch('{product}', ['uses' => 'ProductionController@update', 'as' => 'product.update']);
        $router->get('{product}/delete', ['uses' => 'ProductionController@delete', 'as' => 'product.delete']);
        $router->delete('{product}', ['uses' => 'ProductionController@destroy', 'as' => 'product.destroy']);
    });
});