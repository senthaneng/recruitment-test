<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\ProductStoreRequest;

class ProductionController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $products = Product::get();

            return json_decode($products, true);
        }

        return view('product.index' ,compact('products'));
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(ProductStoreRequest $request)
    {
        $product = product::create($request->only(['name', 'quantity', 'price']));
        return redirect()->route('product.index')->with('message', 'product was successfully created!');
    }

    public function edit(Product $product)
    {
        return view('product.edit', compact('product'));
    }

    public function update(ProductUpdateRequest $request, Product $product)
    {
        $product->update($request->only(['name', 'quantity', 'price']));
        return redirect()->route('product.index')->with('message', 'product was successfully updated!');
    }

    public function delete(Product $product)
    {
        return view('product.delete', compact('product'));
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('product.index')->with('message', 'product was successfully deleted!');
    }

    public function show(Product $product)
    {
        $total = $product->quantity * $product->price;
        return view('product.show', compact('product', 'total'));
    }
}
